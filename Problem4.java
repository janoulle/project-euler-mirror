/**
 * @author  Jane Ullah
 * @purpose Problem 4 - Project Euler
 * @date    12/19/2011
 * @site 	http://janetalkscode.com	
 */
public class Problem4 {

	public static void main(String[] args) {
		/*if (isPalindrome(1222321))	{
			System.out.println("Yes.");
		}*/
		boolean loopControl = false;
		String palindrome = "";
		for (int i = 1000000; !loopControl; i--)	{
			if (isPalindrome(i))	{
				palindrome = valid3DigitDivisor(i);
				if ( !(palindrome == "")) {
					System.out.println("largest palindrome is: " + i);
					loopControl = true;
				}
			}
		}
		
	}
	
	private static boolean isPalindrome(int number){
		//converting the number to a String
		String dummy = "" + number;
		int index;
		//Compares the ends of the String and terminates when a single mismatch is found
		for (index = 1; index <= dummy.length()/2 && ( (dummy.charAt((dummy.length()-index)) == dummy.charAt((index-1)) ) ); index++);
		return (index > dummy.length()/2);
	}
	
	private static String valid3DigitDivisor(int palindrome){
		int j, loopControl = (int) Math.floor(Math.sqrt(palindrome));
		String divisor, quotient, result = "";
		for (j = 0; j <= loopControl; j++)	{
			//Catching divisors of the palindrome
			if ( (palindrome % (j+2) == 0) )	{
				divisor = "" + (j+2);
				quotient = "" + (palindrome/(j+2));
				//Selecting for numbers (divisor and quotient) that are 3 digits
				if ( (divisor.length() == 3) && (quotient.length() == 3) )	{
					result =  divisor + " " + quotient;
				}	
			}
		}
		return result;
	}
}
